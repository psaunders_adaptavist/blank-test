'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

/**
 * Entry point to internal event trigger
 *
 * @param event Object that holds internal event data
 * @param context Object that holds function invocation context data
 */
async function run(event, context) {
    // Print out event and context data
    console.log('Internal event received', event, context);
}

exports.run = run;
