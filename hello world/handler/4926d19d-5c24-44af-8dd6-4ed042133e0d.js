'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

/**
 * Entry point to manual trigger
 *
 * @param event Object that holds manual trigger event data
 * @param context Object that holds function invocation context data
 */
async function run(event, context) {
    // Print out event and context data
    console.log('Manual trigger invoked', event, context);
}

exports.run = run;
