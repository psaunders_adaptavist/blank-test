/**
 * Entry point to manual trigger
 *
 * @param event Object that holds manual trigger event data
 * @param context Object that holds function invocation context data
 */
export async function run(event: ManualTriggerEvent, context: Context) {
    // Print out event and context data
    console.log('Manual trigger invoked', event, context);
}