/**
 * Entry point to internal event trigger
 *
 * @param event Object that holds internal event data
 * @param context Object that holds function invocation context data
 */
export async function run(event: InternalEvent, context: Context) {
    // Print out event and context data
    console.log('Internal event received', event, context);
}

/**
 * TypeScript interface delcaration for defining the shape of internal event that is expected to be received
 * Using TypeScript intefaces is optional, but highly recommended to produce higher quality code, learn more here: https://www.typescriptlang.org/docs/handbook/interfaces.html
 * */
interface InternalEvent {
}